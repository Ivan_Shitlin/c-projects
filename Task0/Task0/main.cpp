//
//  main.cpp
//  Task0
//
//  Created by Иван Шитлин on 08.09.16.
//  Copyright © 2016 Ivan Shitlin. All rights reserved.
//
#include <algorithm>
#include "Header.h"
#include "sort_strings.h"
using namespace std;

int main(int argc, const char * argv[]) {
    ifstream fin;
    ofstream fout;
    fin.open(argv[1]);
    fout.open(argv[2]);
    list<string> str_list;
    string str;
    while (getline(fin,str)){
        str_list.push_back(str);
    }
    sort_strings:: sort_string(str_list);
    
    
    
    return 0;
}
