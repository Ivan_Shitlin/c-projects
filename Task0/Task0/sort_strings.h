//
//  sort_strings.h
//  Task0
//
//  Created by Иван Шитлин on 09.09.16.
//  Copyright © 2016 Ivan Shitlin. All rights reserved.
//

#ifndef sort_strings_h
#define sort_strings_h
namespace sort_strings{
void sort_string (std::list<std::string> str);
}
#endif /* sort_strings_h */
